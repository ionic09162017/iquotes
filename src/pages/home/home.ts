import { Component, OnInit } from '@angular/core';
import { NavController } from 'ionic-angular';
import { QuoteService } from '../../service/quote.service';
import { Quote } from '../../model/quote.model';
import { QuoteDetailsPage } from '../quote-details/quote-details';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements OnInit {
  quotes: Quote[];

  constructor(public navCtrl: NavController, private quoteService: QuoteService) {}

  ngOnInit() {
    this.quoteService.getAll()
      .subscribe(res => this.quotes = res);
  }

  viewDetails(quote) {
    this.navCtrl.push(QuoteDetailsPage, {text: quote.text, from: quote.from})
  }
}
