import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RandomQuotePage } from './random-quote';

@NgModule({
  declarations: [
    RandomQuotePage,
  ],
  imports: [
    IonicPageModule.forChild(RandomQuotePage),
  ],
})
export class RandomQuotePageModule {}
